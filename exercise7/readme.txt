You can perform task like:
1. add task 
2. do the task
3. list your todos

$ task
With task command you can add task to your todo list do the task and view all the task

Usage:
  task [command]

Available Commands:
  add         add command is used to add task into todo
  do          do command is used for doing the task from todo list
  help        Help about any command
  list        list command will list all the task in the todo

Flags:
  -h, --help   help for task

Use "task [command] --help" for more information about a command.

$ task add new task
Added new task new task

$ task add clean dishes
value added at index: 3

$ task list
index:1 value: review talk proposal
index:2 value: some task description
index:3 value: new task

$ task do new task
marked the task as done and removed from the queue

